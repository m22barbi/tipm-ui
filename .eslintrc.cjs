/* eslint-env node */
require("@rushstack/eslint-patch/modern-module-resolution")

module.exports = {
	"root": true,
	"parser": "@typescript-eslint/parser",
	"extends": [
		"plugin:vue/vue3-essential",
		"eslint:recommended",
		"@vue/eslint-config-typescript/recommended",
		"plugin:vue/recommended"
	],
	"env": {
		"vue/setup-compiler-macros": true
	},

	"rules": {
		"indent": ["error", "tab", { "SwitchCase": 1 }],
		"no-mixed-spaces-and-tabs": "error",
		"vue/multi-word-component-names": ["error", { "ignores": [ "Login", "Index" ] }],
		"semi": ["error", "never"],
	},

	"ignorePatterns": [ "public/js/bootstrap.*" ]
}
