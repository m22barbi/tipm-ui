# TIPM - UI

## How to install TIPM-UI

tipm-ui does not include TIPM and PBAD you need to install them yourself.

See: https://gitlab.imt-atlantique.fr/m22barbi/tipm (use this repository and not len fereman's because we added numerous patches and fixes)

### Dependencies:
* Node.js >= 16 (14 might work, but it is not tested)
* npm (often bundled with node)

if the version 16 is not available on your system's repositories you can install any node version and then install the N version manager

run these command to install n
```bash
npm i -g n
n lts
```
Now when you run the command `n` you will get a menu in witch you can select the version of node you want to use and press enter to validate.

### Installation:
clone the Gitlab repository and install node dependencies
```
git clone https://gitlab.imt-atlantique.fr/m22barbi/tipm-ui
cd tipm-ui
npm install
```

## Running TIPM
All you have to do is open a terminal in the TIPM-UI root folder and run :
```
npm start serve
```

## Instruction d'installation

tipm-ui ne fourni pas TIPM and PBAD mais vous devez les installer vous même.

Voire: https://gitlab.imt-atlantique.fr/m22barbi/tipm (Utiliser ce depot et non celui de len fereman vu qu'on a modifié beaucoup de fonctionnalités)

### Dependencies:
* Node.js >= 16 (14 marchera peut-être mais il n'est pas testé)
* npm

si vous n'avez pas la version 16+ disponible sur votre système je vous invite a utiliser N.

lancer ces commandes en administrateur
```
npm i -g n
n lts
```
maintenant quand vous lancer la commende `n`(en administrateur) vous all avoir un menu dans le quel vous pouvez sélectionner la version 16 et valider avec la touche entrer.

### Installation:
Cloner les depots gitlab et installer les dépendances
```
git clone https://gitlab.imt-atlantique.fr/m22barbi/tipm-ui
cd tipm-ui
npm install
```

## Running TIPM
All you have to do is open a terminal in the TIPM-UI root folder and run :
```
npm start serve
```
