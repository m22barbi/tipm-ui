import secureFetch from '@/secureFetch'
import { defineComponent } from 'vue'
import HeaderComponent from '../components/HeaderComponent/HeaderComponent.vue'
import Multiselect from '@vueform/multiselect'


export default defineComponent({
	name: 'App',
	components: {
		HeaderComponent,
		Multiselect
	},
	data() {
		return {
			files: [] as { id: number, name: string, file: Blob}[],
			currentDrag: {} as DragEvent,
			projects: [] as Project[],
			selectedProject: "",
			currentOver: null as number | null
		}
	},
	computed: {
		filesUrls(): {src: string, id: number, name: string}[] {
			const out = [] as {src: string, id: number, name: string}[]
			for(const file of this.files) {
				out.push({
					src: URL.createObjectURL(file.file),
					id: file.id,
					name: file.name
				})
			}
			return out
		}
	},

	async mounted() {
		this.projects = await (await secureFetch(this, import.meta.env.VITE_BACKEND_URL + '/rest/projects')).json()
		this.selectedProject = this.projects[0]?.name
	},

	methods: {
		async uploadEvent(e: Event) {
			//@ts-expect-error target is generic
			const input: HTMLInputElement | null = e.target
			if(!input) return

			//@ts-expect-error FileList is not an array but might be compatible, array.from is the standard way to convert it into arrays
			const files: File[] = Array.from(input.files).filter(file => file.name.endsWith('.tif') || file.name.endsWith('.tiff') || file.name.endsWith('.png'))

			if(input.files?.length != files.length) {
				this.$toast.warning('non tiff or png files were ignored')
			}

			if(!input.files?.length)return this.$toast.error('nothing to upload')

			const formData = new FormData()
			for(const file of files)
				formData.append("files", file)

			this.$toast.info("uploading ...")
			const response = await secureFetch(this, import.meta.env.VITE_BACKEND_URL + '/image/store', { body: formData, method: 'POST' })
			const fileIds: number[] = await response.json()

			for(const fileId of fileIds) {
				const response = await secureFetch(this, import.meta.env.VITE_BACKEND_URL + `/image/${fileId}`)
				const contentDisposition = response.headers.get("Content-Disposition")
				if(!contentDisposition) {
					console.log("warning: incomplete filename")
				}
				this.files.push({ id: fileId,
					//TODO: properly parse the line instead of a unreliable split
					name: contentDisposition?.split("filename=")[1] + "    id: " + fileId.toString() || fileId.toString(),
					file: await response.blob()
				})
			}
			this.$toast.success("upload finished")
		},

		dropping(e: DragEvent) {
			//@ts-expect-error target is generic
			let original: HTMLDivElement | HTMLParagraphElement = this.currentDrag.target
			if(original.nodeName !== 'DIV') {
				//@ts-expect-error P's parent is a div
				original = original.parentNode
			}
			original.style.opacity = '1'

			const originalInput = original.querySelector('input') as HTMLInputElement

			//@ts-expect-error Target is generic
			let newTargetParagraph: HTMLParagraphElement | HTMLDivElement | HTMLInputElement = e.target
			if(newTargetParagraph.nodeName !== 'DIV') {
				newTargetParagraph = newTargetParagraph.parentNode as HTMLDivElement
			}
			if(!newTargetParagraph) return
			const newInput = newTargetParagraph.querySelector('input') as HTMLInputElement

			const oldIndex = this.files.findIndex((val) => val.id.toString() === originalInput.value )
			const newIndex = this.files.findIndex((val) => val.id.toString() === newInput.value)

			//swap elements
			const old = this.files[oldIndex]
			this.files[oldIndex] = this.files[newIndex]
			this.files[newIndex] = old
		},

		dragging(e: DragEvent) {
			//@ts-expect-error target is generic
			const target: HTMLDivElement = e.target
			this.currentDrag = e
			target.style.opacity = '0.5'
			console.log('drag start')
		},

		over(e: Event) {
			e.preventDefault()
		},

		enter(e: Event) {
			e.preventDefault()
			//@ts-expect-error target is generic
			let target: HTMLDivElement | HTMLParagraphElement | null = e.target
			if(!target) return

			if(target.nodeName !== 'DIV') {
				//@ts-expect-error P's parent is a div
				target = target.parentNode
				if(!target)return
			}

			if(this.currentDrag.target === target) return

			target.style.marginLeft = '40px'
		},

		exit(e: Event) {
			//@ts-expect-error target is generic
			let target: HTMLDivElement | HTMLParagraphElement | null = e.target
			if(!target) return
			if(target.nodeName !== 'DIV') {
				//@ts-expect-error P's parent is a div
				target = target.parentNode
				if(!target)return
			}
			if(this.currentDrag.target === target) return
			target.style.marginLeft = "auto"
			e.preventDefault()
		},

		prevent(e: Event) {
			e.preventDefault()
		},

		async kmeans(e: Event) {
			e.preventDefault()

			if(!this.selectedProject) return this.$toast.error("not project selected")

			const form = new FormData(e.target as HTMLFormElement)
			form.append("ids", this.files.map(e => e.id).join(','))
			form.append("maxiterations", '10')
			form.append("project", this.selectedProject)

			await secureFetch(this, import.meta.env.VITE_BACKEND_URL + '/image/cluster', { method: 'POST', body: form }).then(async (response) => {
				if(response.status === 200)this.$router.push('/')
				else this.$toast.error(await response.text() || "someting happed")
			}).catch(e => {
				this.$toast.error(e)
			})

		},

		async kmeansDTW(e: Event) {
			e.preventDefault()

			if(!this.selectedProject) return this.$toast.error("not project selected")

			const form = new FormData(e.target as HTMLFormElement)
			form.append("ids", this.files.map(e => e.id).join(','))
			form.append("maxiterations", '10')
			form.append("project", this.selectedProject)

			await secureFetch(this, import.meta.env.VITE_BACKEND_URL + '/image/dtwcluster', { method: 'POST', body: form }).then((response) => {
				if(response.status === 200)this.$router.push('/')
				else this.$toast.error(response.body + "" || "someting happed")
			}).catch(e => {
				this.$toast.error(e)
			})
		}
	},
})
