import secureFetch from '@/secureFetch'
import { defineComponent } from 'vue'
import HeaderComponent from '../components/HeaderComponent/HeaderComponent.vue'

export default defineComponent({
	name: 'App',
	components: {
		HeaderComponent
	},
	data() {
		return {
			projects: null as Project[] | null,
			currentProject: null as string | null,
			newProjectName: ""
		}
	},
	computed: {
		isReady(): boolean {
			return this.projects != null
		}
	},
	mounted() {
		this.loadProjects()
	},
	methods: {
		async loadProjects() {
			const response = await secureFetch(this, import.meta.env.VITE_BACKEND_URL + '/rest/projects')

			if(response.status != 200) console.log(response)
			else {
				const nameSort = (a: FileItem, b: FileItem)=> a.logicalName > b.logicalName ? 1 : -1
				const versionSort = (a: { version: number }, b: { version: number }) => b.version - a.version
				//sort by version and then sort by name
				// items == fileitem but the server send it twice anyway
				const projects: Project[] = await response.json()
				projects.forEach(e => e.items.sort(versionSort))
				projects.forEach(e => e.fileItems.sort(versionSort))
				projects.forEach(e => e.items.sort(nameSort))
				projects.forEach(e => e.fileItems.sort(nameSort))
				projects.sort((a, b)=> a.name > b.name ? 1 : -1)
				this.projects = projects
			}
		},

		async newProject(event: Event) {
			//on ne submit pas le form
			event.preventDefault()

			if(this.newProjectName) {
				const data = new FormData()
				data.append('name', this.newProjectName)
				await secureFetch(this, import.meta.env.VITE_BACKEND_URL + '/rest/project/create', {
					method: 'POST',
					body: data
				})
				await this.loadProjects()
				this.$toast.success("Done")
			} else {
				this.$toast.error('Please enter a valid name')
			}
		},

		async uploadFile(event: Event) {
			event.preventDefault()
			// @ts-expect-error event.target has a generic type but we know it will be a HTMLFormElement
			const data = new FormData(event.target)
			//ask the server to return use the id of the new file
			data.append('apimode', 'true')

			// @ts-expect-error we know the file will be there
			const file:File | null = data.get('file')
			if(!file || !(file.name.endsWith('.csv') || file.name.endsWith('.arff'))) {
				this.$toast.error('Invalid file format')
				return
			}

			const id = await (await secureFetch(this, import.meta.env.VITE_BACKEND_URL + '/rest/upload', {
				method: 'POST',
				body: data
			})).text()


			//if the file is a csv we need to convert it to arff to use it
			if(file.name.endsWith('.csv')) {
				this.$toast.info("Uploaded, now converting to ARFF")
				const form = new FormData()
				form.append('id', id)
				await secureFetch(this, import.meta.env.VITE_BACKEND_URL + '/rest/transform/convert-to-arff', { method: 'POST', body: form })
				this.$toast.success("Converted")
			} else {
				this.$toast.success("Uploaded")
			}

			this.loadProjects()
		},

		async deleteDataset(id: string) {
			const url = import.meta.env.VITE_BACKEND_URL + '/rest/item/remove?item='+encodeURIComponent(id)
			await secureFetch(this, url, { method: 'POST' })
			this.loadProjects()
		},

		setDataset(id: string) {
			const currentProject = this.projects?.flatMap(e => e.items).find(e => e.id === id )
			localStorage.setItem('datasetId', encodeURIComponent(id))

			if(currentProject) {
				localStorage.setItem('hasWindows', `${currentProject.stackOperations.map(e => e.includes('make-windows')).includes(true)}`)
				localStorage.setItem('isArff', '' + currentProject.arff)
				localStorage.setItem('isImage', '' + ('width' in currentProject))
			}
		}
	}
})
