//there is not typescript exposed for any of the following functions
// so there was no point in using typescript for this file
/**
 * @param {number} max
 */
export default function makeTheSliderDragOnLock(max) {
	const limit = 1000
	const margin = 100
	const slider = document.querySelector(".Slider")
	slider.noUiSlider.on('slide', function( values, handle ) {
		if (values[1] - values[0] > limit) {
			if (handle) {
				slider.noUiSlider.set([parseInt(values[1]) - limit, null])
			} else {
				slider.noUiSlider.set([null, parseInt(values[0]) + limit])
			}
		} else if(values[1] - values[0] < margin) {
			if (handle) {
				if(values[0] <= 0) {
					slider.noUiSlider.set([null, margin])
				}else {
					slider.noUiSlider.set([parseInt(values[1]) - margin, null])
				}
			} else {
				if(values[1] >= max) {
					slider.noUiSlider.set([max - margin, max])
				} else {
					slider.noUiSlider.set([null, parseInt(values[0]) + margin])
				}
			}
		}
	})
}
