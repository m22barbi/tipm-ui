import { defineComponent } from 'vue'
import HeaderComponent from '../components/HeaderComponent/HeaderComponent.vue'
import TableComponent from '@/components/TableComponent/TableComponent.vue'
import secureFetch from '@/secureFetch'

export default defineComponent({
	name: 'TableView',
	components: {
		HeaderComponent,
		TableComponent,
	},
	data() {
		return {
			dataset: null as Table | null,
			currentProject: null as string | null,
			newProjectName: "",
			pageLength: 10,
			windowWidth: 10 as number,
			windows: false
		}
	},
	computed: {
		isReady(): boolean {
			return this.dataset !== null
		},
		hasWindows(): boolean {
			return localStorage.getItem('hasWindows') === 'true'
		},
		nbElements(): number | undefined {
			const totalSize = this.dataset?.totalSize
			if(this.windows && totalSize) {
				return Math.ceil(totalSize / this.windowWidth)
			}
			return totalSize
		}
	},

	watch: {
	},
	async mounted() {
		if(!localStorage.getItem('datasetId')) {
			this.$router.push('/')
		}
		//on ne veut que le nombre de page
		this.dataset = await this.fetchDataset(0,0)
		const stats: Stats[] = await (await secureFetch(this, `${import.meta.env.VITE_BACKEND_URL}/rest/metadata/attribute-and-statistics?id=${localStorage.getItem('datasetId')}` )).json()

		if(this.hasWindows) {
			const windowsAttributes = stats.find(stat => stat.attribute === 'Window')

			if(windowsAttributes) {
				this.windowWidth = parseInt(windowsAttributes['support-first-value'].split(':')[1]) - 1
			}
		}
		this.dataset = await this.fetchDataset(0,0)
	},
	methods: {

		async fetchDataset(from: number, to: number): Promise<Table> {
			const id = localStorage.getItem('datasetId')
			if(id) {
				if(this.windows)
					return ( await secureFetch(this, `${import.meta.env.VITE_BACKEND_URL}/rest/load-data?id=${id}&windowed=${this.windows}&pagination=${from * this.windowWidth}-${to * this.windowWidth}`)).json()
				else
					return ( await secureFetch(this, `${import.meta.env.VITE_BACKEND_URL}/rest/load-data?id=${id}&windowed=${this.windows}&pagination=${from}-${to}`)).json()
			} else {
				this.$router.push('/')
				//this error is thrown but will never have any impact
				throw new Error('Unreachable typescript validation error')
			}
		},

		async fetchPage(from: number, to: number): Promise<Array<number | string>[]> {
			const dataset = await this.fetchDataset(from, to)
			return dataset.rows
		}
	}
})
