import { defineComponent } from 'vue'

export default defineComponent({
	name: 'HeaderComponent',
	components: {
	},
	data() {
		return {
			project: localStorage.getItem('datasetId') || 'No current project'
		}
	},
	computed: {
		isDatasetSelected() {
			return localStorage.getItem('datasetId') !== null
		},

		hasWindows() {
			return localStorage.getItem('hasWindows') === 'true'
		},

		isArff() {
			return localStorage.getItem('isArff') === 'true'
		},

		isImage() {
			return localStorage.getItem('isImage') === 'true'
		}
	},
})
