import { defineComponent, type PropType } from 'vue'

export default defineComponent({
	name: 'TableComponent',
	props: {
		data: { type: Array as PropType<Array<string | number>[]>, required: false, default: null },
		dataCallback: { type: Function as PropType<(from: number, to: number) => ((number | string)[][] | Promise<(number | string)[][]>)>, required: false, default: null },
		pageLength: { type: Number as PropType<number>, required: true },
		nbElements: { type: Number as PropType<number>, required: false, default: null }
	},
	data() {
		return {
			currentPage: 0,
			header: [] as (string | number)[],
			body: [] as (string | number)[][]
		}
	},
	computed: {
		from() {
			return this.currentPage * this.pageLength
		},

		to() {
			return (this.currentPage + 1) * this.pageLength + 1
		},

		nbPages(): number {
			if(this.data)
				return Math.ceil((this.data.length) / this.pageLength)
			else if(this.dataCallback !== null) {
				if(this.nbElements)
					return Math.ceil(this.nbElements / this.pageLength)
				else throw new Error('nbElement need to be define with dataCallback')
			}
			throw new Error('No data was provided to the table component')
		},
	},
	watch: {
		currentPage() {
			this.update()
		}
	},
	mounted() {
		this.update()
	},
	methods: {
		changePage(id: number) {
			this.currentPage = id - 1
		},

		formatNumber(value: number | string): number | string {
			if(typeof(value) === 'number') {
				if(Number.isInteger(value)) {
					return value
				} else {
					return value.toFixed(3)
				}
			} else {
				//is float
				if(value.match(/^((-\d\d*\.\d*)|(\d\d*\.\d*)|(-\d\d*)|(\d\d*))$/)) {
					return this.formatNumber(parseFloat(value))
				} else {
					//is integer
					if(value.match(/^\d\d*$/)) return parseInt(value)
					// we don't know what it is
					return value
				}
			}
		},

		async dataHeadless() {
			let copy: (string | number)[][] | null = null
			if(this.data) {
				copy = this.data.filter((_e, i) => i >= this.from && i < this.to)
			} else if (this.dataCallback) {
				copy = await Promise.resolve(this.dataCallback(this.from, this.to))
			}
			if(!copy) throw new Error('No data was provided to the table component')
			copy.shift()
			return copy
		},

		async head(): Promise<(string | number)[]> {
			if(this.data) {
				return this.data[0]
			} else if(this.dataCallback) {
				return (await Promise.resolve(this.dataCallback(0, 0)))[0]
			} else {
				throw new Error('No data was provided to the table component')
			}
		},

		formatData(data: string | number) {
			const lengthLimit = 15
			let values = data.toString().split(';')
			if(values.length > lengthLimit) {
				values = values.filter((_v, i) => i < lengthLimit)
				values.push('...')
			}
			return values.map(this.formatNumber).join(' ')
		},

		async update() {
			this.header = await this.head()
			this.body = await this.dataHeadless()
		}
	}
})
