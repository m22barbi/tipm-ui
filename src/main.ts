import { createApp } from 'vue'
import { createRouter, createWebHistory } from 'vue-router'
import routes from './router'
import Toaster from "@incuca/vue3-toaster"

const router = createRouter({
	history: createWebHistory(),
	routes
})

const app = createApp({})
app.use(router)
app.use(Toaster)
app.mount("#app")
