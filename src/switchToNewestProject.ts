
export default async function switchToNewestProject(self: { $router: { go: (page: number) => void } }) {
	const datasetId = localStorage.getItem('datasetId')
	if(!datasetId) return
	const projects: Project[] = await(await fetch(import.meta.env.VITE_BACKEND_URL + '/rest/projects')).json()


	for(const project of projects) {
		const dataset = project.fileItems.find(e => e.id === datasetId)

		if(dataset) {
			const datasets = project.fileItems.filter(e => e.logicalName === dataset.logicalName)
			const newDataset = datasets.sort((a, b) => b.version - a.version).shift()
			if(newDataset?.id) {
				localStorage.setItem('datasetId', newDataset.id)
			} else {
				localStorage.removeItem('datasetId')
			}
			break
		}
	}
	//reload the page
	self.$router.go(0)
}
